VERSION 1
REFERENCE MV
COUNT 34
INDEX 0
{
   REFERENCE GAME_VERSION
   TEXT_LANGUAGE1 "Gameversion:"
   TEXT_LANGUAGE2 "Version de la partie:"
   TEXT_LANGUAGE3 "Spielversion:"
   TEXT_LANGUAGE6 "Gameversion:"
   TEXT_LANGUAGE8 "Versi�n del juego:"
}
INDEX 1
{
   REFERENCE GAME_VERSION_BROWSER_INFO
   TEXT_LANGUAGE1 "Show only servers with selected gameversion."
   TEXT_LANGUAGE2 "Pr�sente seulement les serveurs de la version choisie."
   TEXT_LANGUAGE3 "Nur Server mit ausgew�hlter Spielversion anzeigen."
   TEXT_LANGUAGE6 "Show only servers with selected gameversion."
   TEXT_LANGUAGE8 "Mostrar solo los servidores de la versi�n seleccionada."
}
INDEX 2
{
   REFERENCE GAME_VERSION_HOST_INFO
   TEXT_LANGUAGE1 "Start server with the selected gameversion."
   TEXT_LANGUAGE2 "D�marre un serveur de la version choisie."
   TEXT_LANGUAGE3 "Server mit ausgew�hlter Spielversion starten."
   TEXT_LANGUAGE6 "Start server with the selected gameversion."
   TEXT_LANGUAGE8 "Comenzar el servidor con la versi�n seleccionada."
}
INDEX 3
{
   REFERENCE ASPECT_RATIO
   TEXT_LANGUAGE1 "Aspect Ratio:"
   TEXT_LANGUAGE2 "Format:"
   TEXT_LANGUAGE3 "Seitenverh�ltnis:"
   TEXT_LANGUAGE6 "Aspect Ratio:"
   TEXT_LANGUAGE8 "Formato:"
}
INDEX 4
{
   REFERENCE JK2MV
   TEXT_LANGUAGE1 "JK2MV"
   TEXT_LANGUAGE2 "JK2MV"
   TEXT_LANGUAGE3 "JK2MV"
   TEXT_LANGUAGE6 "JK2MV"
   TEXT_LANGUAGE8 "JK2MV"
}
INDEX 5
{
   REFERENCE JK2MV_CONFIGURE_OPTIONS
   TEXT_LANGUAGE1 "JK2MV-specific options."
   TEXT_LANGUAGE2 "Param�tes sp�cifiques de JK2MV"
   TEXT_LANGUAGE3 "JK2MV-spezifische Einstellungen."
   TEXT_LANGUAGE6 "JK2MV-specific options."
   TEXT_LANGUAGE8 "JK2MV-opciones espec�ficas."
}
INDEX 6
{
   REFERENCE RES_NATIVE
   TEXT_LANGUAGE1 "Native"
   TEXT_LANGUAGE2 "Natif"
   TEXT_LANGUAGE3 "Nativ"
   TEXT_LANGUAGE6 "Native"
   TEXT_LANGUAGE8 "Nativo"
}
INDEX 7
{
   REFERENCE DYNAMIC_GLOW
   TEXT_LANGUAGE1 "Dynamic Glow:"
   TEXT_LANGUAGE2 "Lumi�re Dynamique:"
   TEXT_LANGUAGE3 "Dynamic Glow:"
   TEXT_LANGUAGE6 "Dynamic Glow:"
   TEXT_LANGUAGE8 "Brillo Din�mico:"
}
INDEX 8
{
   REFERENCE DYNAMIC_GLOW_INFO
   TEXT_LANGUAGE1 "Much better looking lightsabers, needs a fast graphics card."
   TEXT_LANGUAGE2 "Sabres Laser plus r�alistes, demande une bonne carte graphique."
   TEXT_LANGUAGE3 "Besser aussehende Lichtschwerter, ben�tigt gute Grafikkarte."
   TEXT_LANGUAGE6 "Much better looking lightsabers, needs a fast graphics card."
   TEXT_LANGUAGE8 "Mejor aspecto de los sables de luz, se necesita una tarjeta gr�fica r�pida."
}
INDEX 9
{
   REFERENCE DL_SUGGEST
   TEXT_LANGUAGE1 "The server suggests the following file to download:"
   TEXT_LANGUAGE2 "Le serveur sugg�re de t�l�charger le fichier suivant:"
   TEXT_LANGUAGE3 "Der Server schl�gt folgende Datei zum Download vor:
   TEXT_LANGUAGE6 "The server suggests the following file to download:"
   TEXT_LANGUAGE8 "El servidor recomienda el siguiente archivo para descargar:"
}
INDEX 10
{
   REFERENCE DL_PROTOCOL
   TEXT_LANGUAGE1 "Protocol:"
   TEXT_LANGUAGE2 "Protocol:"
   TEXT_LANGUAGE3 "Protokoll:
   TEXT_LANGUAGE6 "Protocol:"
   TEXT_LANGUAGE8 "Protocolo:"
}
INDEX 11
{
   REFERENCE DL_WANTSFILE
   TEXT_LANGUAGE1 "Do you want to download the file?"
   TEXT_LANGUAGE2 "Voulez-vous t�l�charger ce fichier?"
   TEXT_LANGUAGE3 "Willst du die Datei herunterladen?"
   TEXT_LANGUAGE6 "Do you want to download the file?"
   TEXT_LANGUAGE8 "Deseas descargar el archivo?"
}
INDEX 12
{
   REFERENCE DL_CONTINUE
   TEXT_LANGUAGE1 "Download file and continue connecting."
   TEXT_LANGUAGE2 "T�l�charger le fichier et ensuite se connecter."
   TEXT_LANGUAGE3 "Datei herunterladen und Verbindung aufbauen."
   TEXT_LANGUAGE6 "Download file and continue connecting."
   TEXT_LANGUAGE8 "Descargar el archivo y continuar conectando."
}
INDEX 13
{
   REFERENCE DL_SKIP
   TEXT_LANGUAGE1 "Continue connecting without downloading the file."
   TEXT_LANGUAGE2 "Se connecter sans t�l�charger le fichier."
   TEXT_LANGUAGE3 "Verbindung aufbauen ohne die Datei herunterzuladen."
   TEXT_LANGUAGE6 "Continue connecting without downloading the file."
   TEXT_LANGUAGE8 "Continuar conectando sin descargar el archivo."
}
INDEX 14
{
   REFERENCE DL_ABORT
   TEXT_LANGUAGE1 "Disconnect from server and return to the main menu."
   TEXT_LANGUAGE2 "Se d�connecter du serveur et retourner au menu principal."
   TEXT_LANGUAGE3 "Verbindung trennen und zum Hauptmen� zur�ckkehren."
   TEXT_LANGUAGE6 "Disconnect from server and return to the main menu."
   TEXT_LANGUAGE8 "Desconectarse del servidor y volver al men� principal."
}
INDEX 15
{
   REFERENCE DL_NEVER
   TEXT_LANGUAGE1 "Never"
   TEXT_LANGUAGE2 "Never"
   TEXT_LANGUAGE3 "Nie"
   TEXT_LANGUAGE6 "Never"
   TEXT_LANGUAGE8 "Nunca"
}
INDEX 16
{
   REFERENCE DL_NEVER_INFO
   TEXT_LANGUAGE1 "Continue connecting and never ask me again to download this file."
   TEXT_LANGUAGE2 "Continue connecting and never ask me again to download this file."
   TEXT_LANGUAGE3 "Fortsetzen und diese Datei jetzt und in Zukunft nie herunterladen."
   TEXT_LANGUAGE6 "Continue connecting and never ask me again to download this file."
   TEXT_LANGUAGE8 "Continuar conectando y nunca m�s sugerir este archivo para descargar."
}
INDEX 17
{
   REFERENCE BOTFILTER
   TEXT_LANGUAGE1 "Show bots:"
   TEXT_LANGUAGE2 "Show bots:"
   TEXT_LANGUAGE3 "Bots zeigen:"
   TEXT_LANGUAGE6 "Show bots:"
   TEXT_LANGUAGE8 "Ver bots:"
}
INDEX 18
{
   REFERENCE BOTFILTER_INFO
   TEXT_LANGUAGE1 "Include bots in player count."
   TEXT_LANGUAGE2 "Include bots in player count."
   TEXT_LANGUAGE3 "Bots in Spieleranzahl mitz�hlen."
   TEXT_LANGUAGE6 "Include bots in player count."
   TEXT_LANGUAGE8 "Incluir bots en el conteo de jugadores."
}
INDEX 19
{
   REFERENCE DOWNLOADS
   TEXT_LANGUAGE1 "Downloads"
   TEXT_LANGUAGE2 "T�l�charger"
   TEXT_LANGUAGE3 "Downloads"
   TEXT_LANGUAGE6 "Downloads"
   TEXT_LANGUAGE8 "Descargas"
}
INDEX 20
{
   REFERENCE DOWNLOADS_INFO
   TEXT_LANGUAGE1 "List Downloads / Remove files from blacklist"
   TEXT_LANGUAGE2 "List Downloads / Remove files from blacklist"
   TEXT_LANGUAGE3 "Downloads auflisten / Blockierte Dateien wieder freigeben."
   TEXT_LANGUAGE6 "List Downloads / Remove files from blacklist"
   TEXT_LANGUAGE8 "Lista de descargas / Remover archivos de la lista negra"
}
INDEX 21
{
   REFERENCE DOWNLOADS_BLOCKALL
   TEXT_LANGUAGE1 "Block Downloads:"
   TEXT_LANGUAGE2 "Block Downloads:"
   TEXT_LANGUAGE3 "Downloads blockieren:"
   TEXT_LANGUAGE6 "Block Downloads:"
   TEXT_LANGUAGE8 "Bloquear Descargas:"
}
INDEX 22
{
   REFERENCE DOWNLOADS_BLOCKALL_INFO
   TEXT_LANGUAGE1 "Block all downloads without asking."
   TEXT_LANGUAGE2 "Block all downloads without asking."
   TEXT_LANGUAGE3 "Alle Downloads ohne Nachfrage blockieren."
   TEXT_LANGUAGE6 "Block all downloads without asking."
   TEXT_LANGUAGE8 "Bloquear todas las descargas sin preguntar."
}
INDEX 23
{
   REFERENCE DOWNLOADS_DLINFO
   TEXT_LANGUAGE1 "Details"
   TEXT_LANGUAGE2 "Details"
   TEXT_LANGUAGE3 "Details"
   TEXT_LANGUAGE6 "Details"
   TEXT_LANGUAGE8 "Detalles"
}
INDEX 24
{
   REFERENCE DOWNLOADS_DLINFO_INFO
   TEXT_LANGUAGE1 "Show detailed information for this file."
   TEXT_LANGUAGE2 "Show detailed information for this file."
   TEXT_LANGUAGE3 "Datails zu dieser Datei anzeigen."
   TEXT_LANGUAGE6 "Show detailed information for this file."
   TEXT_LANGUAGE8 "Mostrar informaci�n detallada del archivo"
}
INDEX 25
{
   REFERENCE DOWNLOADS_DLREMOVE
   TEXT_LANGUAGE1 "Delete"
   TEXT_LANGUAGE2 "Delete"
   TEXT_LANGUAGE3 "L�schen"
   TEXT_LANGUAGE6 "Delete"
   TEXT_LANGUAGE8 "Eliminar"
}
INDEX 26
{
   REFERENCE DOWNLOADS_DLREMOVE_INFO
   TEXT_LANGUAGE1 "Remove file / blacklist entry."
   TEXT_LANGUAGE2 "Remove file / blacklist entry."
   TEXT_LANGUAGE3 "Datei l�schen / Blockierung aufheben."
   TEXT_LANGUAGE6 "Remove file / blacklist entry."
   TEXT_LANGUAGE8 "Eliminar archivo / a�adir a la lista negra"
}
INDEX 27
{
   REFERENCE DOWNLOADS_DLPERMANENT
   TEXT_LANGUAGE1 "Make Permanent"
   TEXT_LANGUAGE2 "Make Permanent"
   TEXT_LANGUAGE3 "Immer laden"
   TEXT_LANGUAGE6 "Make Permanent"
   TEXT_LANGUAGE8 "Hacer Permanente"
}
INDEX 28
{
   REFERENCE DOWNLOADS_DLPERMANENT_INFO
   TEXT_LANGUAGE1 "Always load this file when JK2 starts."
   TEXT_LANGUAGE2 "Always load this file when JK2 starts."
   TEXT_LANGUAGE3 "Datei immer laden wenn JK2 startet."
   TEXT_LANGUAGE6 "Always load this file when JK2 starts."
   TEXT_LANGUAGE8 "Cargar siempre este archivo cuando inicie JK2."
}
INDEX 29
{
   REFERENCE DOWNLOADS_INFOTITLE
   TEXT_LANGUAGE1 "File Info"
   TEXT_LANGUAGE2 "File Info"
   TEXT_LANGUAGE3 "Datei Informationen"
   TEXT_LANGUAGE6 "File Info"
   TEXT_LANGUAGE8 "Informaci�n del archivo"
}
INDEX 30
{
   REFERENCE NAME_SHADOWS
   TEXT_LANGUAGE1 "Colored Shadows:"
   TEXT_LANGUAGE2 "Ombres Color�es:"
   TEXT_LANGUAGE3 "Farbige Schatten:"
   TEXT_LANGUAGE6 "Colored Shadows:"
   TEXT_LANGUAGE8 "Sombras Coloreadas:"
}
INDEX 31
{
   REFERENCE NAME_SHADOWS_INFO
   TEXT_LANGUAGE1 "Colored shadows in names."
   TEXT_LANGUAGE2 "Ombres color�es dans les noms."
   TEXT_LANGUAGE3 "Farbige schatten in namen."
   TEXT_LANGUAGE6 "Colored shadows in names."
   TEXT_LANGUAGE8 "Sombras Coloreadas en los nombres."
}
INDEX 32
{
   REFERENCE CONSOLE_SHIFT
   TEXT_LANGUAGE1 "Console Shift Key:"
   TEXT_LANGUAGE2 "Console Maj Cl�:"
   TEXT_LANGUAGE3 "Konsole Shift-Taste:"
   TEXT_LANGUAGE6 "Console Shift Key:"
   TEXT_LANGUAGE8 "Tecla Shift Consola:"
}
INDEX 33
{
   REFERENCE CONSOLE_SHIFT_INFO
   TEXT_LANGUAGE1 "Shift key required to open the console."
   TEXT_LANGUAGE2 "Maj cl� n�cessaire pour ouvrir la console."
   TEXT_LANGUAGE3 "Shift-Taste erforderlich, um die konsole zu �ffnen."
   TEXT_LANGUAGE6 "Shift key required to open the console."
   TEXT_LANGUAGE8 "Tecla Shift necesaria para abrir la consola."
}